package net.nilosplace.blink.interfaces;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import net.nilosplace.blink.model.BlinkPatterns;
import net.nilosplace.blink.model.BlinkStatus;
import net.nilosplace.blink.model.PatternStatus;

@Path("/pattern")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface BlinkPatternRESTInterface {

    @GET
    public BlinkPatterns getPatterns();
    
    @GET
    @Path("/play")
    public PatternStatus playPattern(
        @QueryParam("pname") String patternName,
        @QueryParam("blink1Id") String blink1Id
    );
    
    @GET
    @Path("/stop")
    public PatternStatus stopPattern(
        @QueryParam("pname") String patternName
    );
    
    @GET
    @Path("/stop")
    public PatternStatus stopAllPatterns();
    
    @GET
    @Path("/add")
    public BlinkStatus addPattern(
        @QueryParam("name") String name,
        @QueryParam("pattern") String pattern
    );
    
    @GET
    @Path("/del")
    public BlinkStatus deletePattern(
        @QueryParam("name") String name,
        @QueryParam("id") String id
    );
    
}
