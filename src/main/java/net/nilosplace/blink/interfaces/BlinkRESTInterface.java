package net.nilosplace.blink.interfaces;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import net.nilosplace.blink.model.BlinkInfo;
import net.nilosplace.blink.model.BlinkStatus;
import net.nilosplace.blink.model.FadeResponse;
import net.nilosplace.blink.model.LastColorResponse;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public interface BlinkRESTInterface {

    @GET
    @Path("/id")
    public BlinkInfo getId();
    
    @GET
    @Path("/fadeToRGB")
    public FadeResponse fadeToRGB(
        @QueryParam("rgb") String rgb,
        @QueryParam("time") Double time,
        @QueryParam("id") String id,
        @QueryParam("ledn") Integer ledn
    );
    
    @GET
    @Path("/lastColor")
    public LastColorResponse lastColor();
    
    @GET
    @Path("/off")
    public BlinkStatus turnOff();
    
    @GET
    @Path("/on")
    public BlinkStatus turnOn();
    
}
