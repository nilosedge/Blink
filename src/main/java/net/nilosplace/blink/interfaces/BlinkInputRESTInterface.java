package net.nilosplace.blink.interfaces;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import net.nilosplace.blink.model.BlinkInputs;

@Path("/input")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface BlinkInputRESTInterface {

    @GET
    public BlinkInputs getInputs();

}
