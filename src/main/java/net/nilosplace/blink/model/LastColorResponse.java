package net.nilosplace.blink.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class LastColorResponse {
    
    private String lastColor;
    private String status;
}
