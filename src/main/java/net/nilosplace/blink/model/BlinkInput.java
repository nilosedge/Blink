package net.nilosplace.blink.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class BlinkInput {

    private String type;
    private Boolean enabled;
    private String name;
    private String actionType;
    private String patternId;
    private String blink1Id;
    
}
