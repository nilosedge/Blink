package net.nilosplace.blink.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class BlinkPatterns {

    private List<BlinkPattern> patterns;
    private String status;
}
