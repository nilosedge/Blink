package net.nilosplace.blink.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class FadeResponse {

    private List<String> blink1Serials;
    private String lastColor;
    private Long lastTime;
    private Integer lastLedn;
    private String cmd;

}
