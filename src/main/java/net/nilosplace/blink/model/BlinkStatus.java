package net.nilosplace.blink.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class BlinkStatus {

    private String status;
}
