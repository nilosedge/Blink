package net.nilosplace.blink.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PatternStatus {

    private String status;
    private String pname;
    private String blink1id;
}
