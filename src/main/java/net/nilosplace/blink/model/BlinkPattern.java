package net.nilosplace.blink.model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class BlinkPattern {

    private String name;
    private String id;
    private String pattern;
    private Boolean locked;

}
