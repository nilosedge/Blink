package net.nilosplace.blink.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class BlinkInfo {

    private List<String> blink1_serialnums;
    private String blink1_id;
    private String status;
    
}
