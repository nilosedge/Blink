package net.nilosplace.blink.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class BlinkInputs {

    private List<BlinkInput> inputs;
    private String status;
}
