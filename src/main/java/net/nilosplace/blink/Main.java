package net.nilosplace.blink;

import java.util.concurrent.LinkedBlockingQueue;

import net.nilosplace.blink.config.ConfigHelper;
import net.nilosplace.blinkster.model.BlinkPatternModel;
import net.nilosplace.blinkster.threads.BlinkManager;
import net.nilosplace.blinkster.threads.GithubWatcher;
import net.nilosplace.blinkster.threads.GoogleCalendarWatcher;

public class Main {

    public static void main(String[] args) throws Exception {
        ConfigHelper.init();
        
        LinkedBlockingQueue<BlinkPatternModel> patternQueue = new LinkedBlockingQueue<BlinkPatternModel>();
        
        BlinkManager manager = new BlinkManager(patternQueue);
        manager.start();

        GithubWatcher watcher = new GithubWatcher(patternQueue);
        watcher.start();
        
        GoogleCalendarWatcher googleWatcher = new GoogleCalendarWatcher(patternQueue);
        googleWatcher.start();

    }

}
