package net.nilosplace.blink.impl;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

import net.nilosplace.blink.model.HSLColor;

public class BlinkFrame extends JFrame {

    private Color lastColor = Color.BLACK;
    private final JPanel right = new JPanel();
    private final JPanel left = new JPanel();


    public BlinkFrame() {
        GridLayout layout = new GridLayout(0,2);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(layout);

        left.setBackground(Color.BLACK);
        right.setBackground(Color.BLACK);
        add(left);
        add(right);
        setSize(300, 300);
        setVisible(true);
    }

    public void fadeToRGB(String rgb, Double time, Integer led) {
        Color newColor = convertColor(rgb);

        int steps = 10;

        HSLColor newHSL = new HSLColor(newColor);
        HSLColor oldHSL = new HSLColor(lastColor);

        double incH = ( newHSL.getHue() - oldHSL.getHue() ) / ( steps - 1 );
        double incS = ( newHSL.getSaturation() - oldHSL.getSaturation() ) / ( steps - 1 );
        double incL = ( newHSL.getLuminance() - oldHSL.getLuminance() ) / ( steps - 1 );

        //System.out.println("IH: " + incH + " IS: " + incS + " IL: " + incL + " Led: " + led);

        for(int i = 0; i < steps; i++) {
            HSLColor temp = new HSLColor(
                (float)(oldHSL.getHue() + (i * incH)),
                (float)(oldHSL.getSaturation() + (i * incS)),
                (float)(oldHSL.getLuminance() + (i * incL))
            );

            if(led == 0) {
                left.setBackground(temp.getRGB());
                right.setBackground(temp.getRGB());
            } else if(led == 1) {
                left.setBackground(temp.getRGB());
            } else if(led == 2) {
                right.setBackground(temp.getRGB());
            }
            try {
                //Thread.sleep(1000);
                Thread.sleep((long)((time * 1000) / steps));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        lastColor = newColor;
    }



    public Color convertColor(String hexString) {
        hexString = hexString.replace("#", "");
        return new Color(
                Integer.valueOf( hexString.substring( 0, 2 ), 16 ),
                Integer.valueOf( hexString.substring( 2, 4 ), 16 ),
                Integer.valueOf( hexString.substring( 4, 6 ), 16 ));
    }
}